const mongoose = require('mongoose');

const produtoSchema = new mongoose.Schema({
    codigo: String,
    nome: {
        type: String,
        required: true
    },
    preco: Number
}, {timestamps: true});
const produto = mongoose.model('produtos', produtoSchema);

module.exports = produto;